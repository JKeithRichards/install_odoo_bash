#!/bin/bash

if [ "$EUID" -ne 0 ]
then
    echo "Please run as root"
    exit
fi

echo "It's all good!"

wget -O - https://nightly.odoo.com/odoo.key | apt-key add -
sed -ri '/odoo/d' /etc/apt/sources.list
echo "deb http://nightly.odoo.com/9.0/nightly/deb/ ./" >> /etc/apt/sources.list
#apt-get update && apt-get install odoo

